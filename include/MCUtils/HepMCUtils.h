// -*- C++ -*-
//
// This file is part of MCUtils -- https://bitbucket.org/andybuckley/mcutils
// Copyright (C) 2013-2016 Andy Buckley <andy.buckley@cern.ch>
//
// Embedding of MCUtils code in other projects is permitted provided this
// notice is retained and the MCUtils namespace and include path are changed.
//
#ifndef MCUTILS_HEPMCUTILS_H
#define MCUTILS_HEPMCUTILS_H

/// @file Convenience header for including all HepMC tool functionality
/// @author Andy Buckley <andy.buckley@cern.ch>

#include "MCUtils/HepMCVectors.h"
#include "MCUtils/HepMCParticleClassifiers.h"
#include "MCUtils/HepMCParticleFilters.h"
#include "MCUtils/HepMCParticleUtils.h"
#include "MCUtils/HepMCVertexClassifiers.h"
#include "MCUtils/HepMCVertexUtils.h"
#include "MCUtils/HepMCEventUtils.h"
#include "MCUtils/HepMCEventFilters.h"

#endif

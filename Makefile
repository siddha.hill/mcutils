## Makefile for MCUtils
## Execute as 'make HEPUTILS_PREFIX=/path/to/heputils HEPMC_PREFIX=/path/to/hepmc', 'make install', 'make clean'

CXX := g++
CXXFLAGS := -O2 -std=c++11

VERSION := 1.3.4

.PHONY = dummy check install uninstall dist clean distclean doxy

dummy:
	$(warning No default make target: use 'make install' to install HEPUtils headers)


ifndef HEPUTILS_PREFIX
hepmcreduce:
	$(error $$HEPUTILS_PREFIX is not set: run like 'make hepmcreduce HEPUTILS_PREFIX=/path/to/heputils HEPMC_PREFIX=/path/to/hepmc')
else ifndef HEPMC_PREFIX
hepmcreduce:
	$(error $$HEPMC_PREFIX is not set: run like 'make hepmcreduce HEPUTILS_PREFIX=/path/to/heputils HEPMC_PREFIX=/path/to/hepmc')
else
hepmcreduce: hepmcreduce.cc $(wildcard include/MCUtils/*.h)
	$(CXX) $(CXXFLAGS) -o hepmcreduce hepmcreduce.cc \
      -Iinclude -I$(HEPMC_PREFIX)/include -I$(HEPUTILS_PREFIX)/include \
      -L$(HEPMC_PREFIX)/lib -lHepMC `fastjet-config --cxxflags --libs` -Wall -pedantic
endif


ifndef HEPUTILS_PREFIX
testpid:
	$(error $$HEPUTILS_PREFIX is not set: run like 'make testpid HEPUTILS_PREFIX=/path/to/heputils')
else
testpid: testpid.cc $(wildcard include/MCUtils/*.h)
	$(CXX) $(CXXFLAGS) -o testpid testpid.cc \
      -Iinclude -I$(HEPUTILS_PREFIX)/include \
      -Wall -pedantic
endif


check: testpid
	./check-testpids.sh

HepMCsubrep:
	if [ -d HepdMC ]; then echo 'HepMC Exists'; else git submodule add https://gitlab.cern.ch/hepmc/HepMC.git; fi
HepMC3subrep:
	if [ -d HepdMC3 ]; then echo 'HepMC3 Exists'; else git submodule add https://gitlab.cern.ch/hepmc/HepMC3.git; fi
HepUtilssubrep:
	if [ -d HepUtils ]; then echo 'HepUtils Exists'; else git submodule add https://gitlab.com/hepcedar/heputils.git; fi
MCUtilssubrep:
	if [ -d mcutils ]; then echo 'mcutils Exists'; else git submodule add https://gitlab.com/hepcedar/mcutils.git; fi


testHepMC3:
	$(CXX) -DMCUTILS_HEPMC3 -I./mcutils/include -I./heputils/include -I./HepMC3/include -I./HepMC ./tests/HepFV2P4.cc -o HepFV2P4
	./HepFV2P4
	$(CXX) -DMCUTILS_HEPMC3 -lHepMC3 -I./mcutils/include -I./heputils/include -I./HepMC3/include -I./HepMC ./tests/childrenParticleUtils.cc -o childrenParticleUtils
	./childrenParticleUtils
	$(CXX) -DMCUTILS_HEPMC3 -lHepMC3 -I./mcutils/include -I./heputils/include -I./HepMC3/include -I./HepMC ./tests/parentsParticleUtils.cc -o parentsParticleUtils
	./parentsParticleUtils
testHepMC:
	$(CXX) -I./include -lHepMC -I./HepMC3/include -I./HepMC/include ./tests/HepFV2P4.cc -o HepFV2P4
	./HepFV2P4

ifndef PREFIX
install:
	$(error $$PREFIX is not set: run like 'make install PREFIX=/path/to/hepmc')
else
install:
	mkdir -p $(PREFIX) && cp -r include $(PREFIX)
endif


ifndef PREFIX
uninstall:
	$(error $$PREFIX is not set: run like 'make uninstall PREFIX=/path/to/hepmc')
else
uninstall:
	rm -rf $(PREFIX)/include/MCUtils
endif


doxy:
	doxygen

dist: doxy
	tar czf MCUtils-$(VERSION).tar.gz README TODO ChangeLog Makefile Doxyfile doxygen include hepmcfiles

clean:
	rm -f testpid
	rm -f hepmcreduce
	rm -rf doxygen

distclean: clean
	rm -f MCUtils-$(VERSION).tar.gz

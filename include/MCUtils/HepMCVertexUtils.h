// -*- C++ -*-
//
// This file is part of MCUtils -- https://bitbucket.org/andybuckley/mcutils
// Copyright (C) 2013-2016 Andy Buckley <andy.buckley@cern.ch>
//
// Embedding of MCUtils code in other projects is permitted provided this
// notice is retained and the MCUtils namespace and include path are changed.
//
#ifndef MCUTILS_HEPMCVERTEXUTILS_H
#define MCUTILS_HEPMCVERTEXUTILS_H

/// @file Functions for filtering and classifying HepMC GenVertex objects
/// @author Andy Buckley <andy.buckley@cern.ch>


#include <vector>
#ifdef MCUTILS_HEPMC3
#include "HepMC3/GenEvent.h"
#include "HepMC3/GenVertex.h"
#else
#include "HepMC/GenEvent.h"
#endif
#include "HEPUtils/Vectors.h"
#include "HEPUtils/Utils.h"
#include "MCUtils/HepMCEventUtils.h"

namespace MCUtils {


#ifdef MCUTILS_HEPMC3
/// @name Friendlier HepMC type typedefs
//@{
typedef std::vector<HepMC3::GenVertexPtr> GenVertices;
typedef std::vector<HepMC3::ConstGenVertexPtr> ConstGenVertices;
//@}


/// @name Vertex constness converters
///
/// @note These are just syntactic sugar: this is already not so inconvenient!
//@{
inline HepMC3::ConstGenVertexPtr mk_const(HepMC3::GenVertexPtr gv) {
    return std::const_pointer_cast<const HepMC3::GenVertex>(gv);
}
inline HepMC3::GenVertexPtr mk_unconst(HepMC3::ConstGenVertexPtr const_gv) {
    return std::const_pointer_cast<HepMC3::GenVertex>(const_gv);
}
//@}


/// @name Container constness converters
///
/// @note Pointers in containers will still point to the same objects after
/// const conversion!
///
/// @note Due to constness rules, new vectors have to be made and
/// populated. So this is not super-efficient, but seems to be necessary.
//@{

inline ConstGenVertices mk_const(GenVertices& gvs) {
    ConstGenVertices rtn;
    rtn.reserve(gvs.size());
    for (auto gv : gvs) {
        rtn.push_back(std::const_pointer_cast<const HepMC3::GenVertex>(gv));
    }
    return rtn;
}

inline GenVertices mk_unconst(const ConstGenVertices& const_gps) {
    GenVertices rtn;
    rtn.reserve(const_gps.size());
    for (auto const_gp : const_gps) {
        rtn.push_back(std::const_pointer_cast<HepMC3::GenVertex>(const_gp));
    }
    return rtn;
}

//@}

#else
/// @name Friendlier HepMC type typedefs
//@{
typedef std::vector<HepMC::GenVertex*> GenVertices;
typedef std::vector<const HepMC::GenVertex*> GenVerticesC;
//@}


/// @name Vertex constness converters
///
/// @note These are just syntactic sugar: this is already not so inconvenient!
//@{
inline const HepMC::GenVertex* mk_const(HepMC::GenVertex* gv) {
    return const_cast<const HepMC::GenVertex*>(gv);
}
inline HepMC::GenVertex* mk_unconst(const HepMC::GenVertex* const_gv) {
    return const_cast<HepMC::GenVertex*>(const_gv);
}
//@}


/// @name Container constness converters
///
/// @note Pointers in containers will still point to the same objects after
/// const conversion!
///
/// @note Due to constness rules, new vectors have to be made and
/// populated. So this is not super-efficient, but seems to be necessary.
//@{

inline GenVerticesC mk_const(const GenVertices& gvs) {
    GenVerticesC rtn;
    rtn.reserve(gvs.size());
    for (const HepMC::GenVertex* gv : gvs) {
        rtn.push_back(gv);
    }
    return rtn;
}

inline GenVertices mk_unconst(const GenVerticesC& const_gps) {
    GenVertices rtn;
    rtn.reserve(const_gps.size());
    for (const HepMC::GenVertex* const_gp : const_gps) {
        rtn.push_back(const_cast<HepMC::GenVertex*>(const_gp));
    }
    return rtn;
}

//@}

#endif

}
#endif

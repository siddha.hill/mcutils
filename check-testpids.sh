#! /usr/bin/env bash
cat pids.dat | while read l; do
    id=`echo $l | cut -f1 -d" "`
    echo $l
    ./testpid $id
    echo
done > testpids.out
diff testpids{_ref,}.out
exit $?
